/**
 * Created on 21.08.16.
 */


jQuery(document).ready(function(){
    jQuery('.ask-question form').hide();
});

jQuery('.ask-question .question-btn').click(function(){
    jQuery(this).parent().find('form').show(300);
    jQuery(this).toggle(200);
});

jQuery('.panel-title a').click(function() {
    jQuery(this).parent().parent().find('i').removeClass("fa-minus");
    jQuery(this).parent().parent().find('i').addClass("fa-plus");
});

if (jQuery('#reviews-carousel').length){
    jQuery('#reviews-carousel').flickity({
        cellAlign: 'left',
        contain: true,
        pageDots: false,
        arrowShape: {
            x0: 10,
            x1: 60, y1: 50,
            x2: 60, y2: 45,
            x3: 15
        }
    });
}

if (jQuery('#video-reviews-carousel').length){
    jQuery('#video-reviews-carousel').flickity({
        cellAlign: 'left',
        contain: true,
        pageDots: false,
        arrowShape: {
            x0: 10,
            x1: 60, y1: 50,
            x2: 60, y2: 45,
            x3: 15
        }
    });
}